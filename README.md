Improved version of the famous fetchmail.pl script from github.com/mailserver2/mailserver ( which origin is   github.com/hardware/mailserver )

## Features
* multihreaded
* gets openssl fingerprint before fetch
* supports rspamc delivery to mailbox
* supports dovecot deliver fallback if rspamc is missing
* supports mda override via database ( like the original )
* supoorts CATCHALL if the `mda` field in `fetchmail` database is set to `CATCHALL` ( e.g. SQL>`update fetchmail set mda="CATCHALL" where src_user="catchall@some.domain.lan";` ) 
* reports stating and ending of each thread to syslog





---

<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/the-foundation/fetchmail-improved/README.md/logo.jpg" width="480" height="270"/></div></a>


## Todo
- [ ] MORE DOCUMENTATION
- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

